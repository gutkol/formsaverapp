﻿using FormApp.Classes.Model;
using FormApp.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XmlLibrary
{
    public class Xml : ISerializePlugin
    {
        public string Name => "XmlLibrary";

        public string Description => "Plugin ten służy do zapisu i odczytu pliku w formacie xml.";

        public Person Deserialize()
        {
            Person person = new Person();

            string filePath = Path.Combine(Paths.XML_FOLDER_PATH);
            string[] files = null;

            try
            {
                files = Directory.GetFiles(filePath);
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory(Paths.XML_FOLDER_PATH);
            }

            if (files != null)
            {
                string lastFile = files[files.Length - 1].Split('\\').LastOrDefault();

                XmlSerializer deserializer = new XmlSerializer(typeof(Person));

                using (FileStream fileStream = new FileStream(Paths.XML_FOLDER_PATH + lastFile, FileMode.Open))
                {
                    person = (Person)deserializer.Deserialize(fileStream);
                }
            }

            return person;
        }

        public void Serialize(Person person)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Person));

            using (TextWriter textWriter = new StreamWriter(Paths.XML_FOLDER_PATH + person.ID + ".xml"))
            {
                serializer.Serialize(textWriter, person);
            }
        }
    }
}
