﻿using FormApp.Classes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormApp.Interfaces
{
    public interface ISerializePlugin
    {
        string Name { get; }

        string Description { get; }

        void Serialize(Person person);

        Person Deserialize();
    }
}
