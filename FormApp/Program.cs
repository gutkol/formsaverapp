﻿using FormApp.Classes;
using FormApp.Classes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Witamy w formularzu\n");

            ManagerImpl manager = new ManagerImpl();
            int optionMenu = 0;

            do
            {
                TextMenu();

                try
                {
                    optionMenu = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.Clear();
                    Console.WriteLine("Niepoprawny wybór\n");
                    optionMenu = -1;
                }

                switch (optionMenu)
                {
                    case 1:
                        manager.NewForm();
                        Console.Clear();

                        break;

                    case 2:
                        manager.ReadLastForm();

                        Console.WriteLine("\nKliknij dowolny przycisk aby powrócić do menu");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }

            } while (optionMenu != 0);            
        }

        static void TextMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Wybierz którąś z poniższych opcji:");
            Console.WriteLine("1. Wypełnij nowy formularz.");
            Console.WriteLine("2. Wczytaj ostatnio zapisany formularz.");
            Console.WriteLine("0. Wyjście");
            Console.ResetColor();
        }
    }
}
