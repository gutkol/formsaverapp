﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormApp.Classes.Model
{
    public static class Paths
    {
        public const string XML_FOLDER_PATH = @"c:\Formularze\Xml\";

        public const string JSON_FOLDER_PATH = @"c:\Formularze\Json\";

        public const string XML_DLL_PATH = @"C:\Users\radoslaw.nowak\source\repos\FormApp\XmlLibrary\bin\Debug\XmlLibrary.dll";

        public const string JSON_DLL_PATH = @"C:\Users\radoslaw.nowak\source\repos\FormApp\JsonLibrary\bin\Debug\JsonLibrary.dll";

    }
}
