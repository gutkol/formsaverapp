﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormApp.Classes.Model
{
    public class Person
    {
        public int ID { get; set; }

        [PersonDescriptionAttribute("Proszę podać imię")]
        public string Name { get; set; }

        [PersonDescriptionAttribute("Proszę podać nazwisko")]
        public string Surname { get; set; }

        [PersonDescriptionAttribute("Proszę podać wiek")]
        public int Age { get; set; }

        [PersonDescriptionAttribute("Proszę podać date urodzenia")]
        public DateTime DateOfBirth { get; set; }

        [PersonDescriptionAttribute("Proszę wybrać płeć\n1. Mężczyzna\n2. Kobieta")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Sex Sex { get; set; }

        [PersonDescriptionAttribute("Proszę wybrać czy jesteś żonaty/zamężna\n Tak wpisz T/t, Nie wpisz N/n")]
        public bool IsMarried { get; set; }
    }

    public enum Sex
    {
        Mężczyzna,
        Kobieta
    }
}
