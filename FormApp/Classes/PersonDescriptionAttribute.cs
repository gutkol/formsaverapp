﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormApp.Classes
{
    [AttributeUsage(AttributeTargets.All)]
    sealed class PersonDescriptionAttribute : Attribute
    {
        public string Question { get; set; }

        public PersonDescriptionAttribute(string question)
        {
            Question = question;
        }
    }
}
