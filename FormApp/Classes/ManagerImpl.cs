﻿using FormApp.Classes.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FormApp.Classes
{
    class ManagerImpl
    {
        static readonly Person person;

        public Person Person
        {
            get
            {
                return person;
            }
        }


        static ManagerImpl()
        {
            person = new Person();
        }

        public void NewForm()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Nowy formularz");
            Console.ResetColor();

            var type = typeof(Person);

            foreach (var mInfo in type.GetProperties())
            {
                foreach (var attr in Attribute.GetCustomAttributes(mInfo))
                {
                    if (attr is PersonDescriptionAttribute)
                    {
                        var questionAttr = (PersonDescriptionAttribute)attr;
                        Console.WriteLine(questionAttr.Question);

                        string answer = Console.ReadLine();
                        Console.WriteLine();

                        try
                        {
                            Type typeOfProperty = mInfo.PropertyType;

                            if (typeOfProperty.Equals(typeof(string)))
                            {
                                mInfo.SetValue(person, answer.Substring(0, 1).ToUpper() + answer.Substring(1));
                            }
                            else if (typeOfProperty.Equals(typeof(int)))
                            {
                                mInfo.SetValue(person, Convert.ToInt32(answer));
                            }
                            else if (typeOfProperty.Equals(typeof(DateTime)))
                            {
                                DateTime dob;
                                dob = DateTime.ParseExact(answer, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);

                                mInfo.SetValue(person, dob);
                            }
                            else if (typeOfProperty.Equals(typeof(Enum)))
                            {
                                mInfo.SetValue(person, (Convert.ToInt32(answer) - 1 == 0 ? Sex.Mężczyzna : Sex.Kobieta));
                            }
                            else if (typeOfProperty.Equals(typeof(bool)))
                            {
                                bool wichAnswer = false;

                                if (answer.First().Equals('T') || answer.First().Equals('t'))
                                {
                                    wichAnswer = true;
                                }

                                mInfo.SetValue(person, wichAnswer);
                            }

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("UPS cos do dupy poszło: " + ex.Message);
                        }
                    }
                }
            }

            Console.WriteLine("Proszę wybrać do jakiego formatu chcesz zapisać formularz:");
            Console.WriteLine("1. Do pliku xml");
            Console.WriteLine("2. Do pliku json");

            int convertChoiseMenu = 0;

            do
            {
                try
                {
                    convertChoiseMenu = Convert.ToInt32(Console.ReadLine());

                    if (!(convertChoiseMenu >= 1 && convertChoiseMenu <= 2))
                        Console.WriteLine("Proszę wpisać 1 lub 2!");
                    else if (convertChoiseMenu == 1)
                    {
                        person.ID = GetLastFormNumber(Paths.XML_FOLDER_PATH) + 1;

                        SaveXmlFile(person);
                    }
                    else if (convertChoiseMenu == 2)
                    {
                        person.ID = GetLastFormNumber(Paths.JSON_FOLDER_PATH) + 1;

                        SaveJsonFile(person);
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Proszę wpisać 1 lub 2!");
                    convertChoiseMenu = -1;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Jakiś nieznany problem");
                }

            } while (!(convertChoiseMenu >= 1 && convertChoiseMenu <= 2));
        }

        private int GetLastFormNumber(string path)
        {
            string[] files = null;
            string filePath = Path.Combine(path);

            try
            {
                files = Directory.GetFiles(filePath);
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory(filePath);
            }

            if (files != null && files.Length > 0)
            {
                string lastFilenumber = files[files.Length - 1].Split('\\').LastOrDefault();
                FileInfo fileInfo = new FileInfo(lastFilenumber);

                int number = Convert.ToInt32(lastFilenumber.Remove(lastFilenumber.Length - fileInfo.Extension.Length, fileInfo.Extension.Length));

                return number;
            }
            else
                return 0;
        }
        private Person GetLatestFormSaved(string[] paths)
        {
            Dictionary<DateTime, string> listOfPaths = new Dictionary<DateTime, string>(paths.Length);
            Person lastForm = null;

            foreach(string path in paths)
            {
                string filePath = Path.Combine(path);
                string[] files = null;

                try
                {
                    files = Directory.GetFiles(filePath);

                }
                catch (DirectoryNotFoundException)
                {
                    Directory.CreateDirectory(path);
                }

                if (files != null && files.Length > 0)
                {
                    string lastFile = files[files.Length - 1];

                    FileInfo fileInfo = new FileInfo(lastFile);

                    listOfPaths.Add(fileInfo.CreationTime, fileInfo.FullName);
                }
                else
                    listOfPaths.Add(new DateTime(0), null);
            }

            string pathToLoad = listOfPaths.OrderByDescending(x => x.Key).First().Value;

            FileInfo file = new FileInfo(pathToLoad);

            if (file.Extension == ".xml")
                lastForm = ReadXmlFile();
            else if (file.Extension == ".txt")
                lastForm = ReadJsonFile();

            return lastForm;
        }

        public void ReadLastForm()
        {
            Console.Clear();

            Person lastPerson = null;

            //DateTime JsonLastForm = GetLastFormNumber(Paths.JSON_FOLDER_PATH);
            //DateTime XmlLastForm = GetLastFormNumber(Paths.XML_FOLDER_PATH);

            //if (JsonLastForm >= XmlLastForm)
            //{
            //    lastPerson = ReadJsonFile();
            //}
            //else if (XmlLastForm >= JsonLastForm)
            //    lastPerson = ReadXmlFile();
            //else
            //    Console.WriteLine("Brak ostatnio dodanego formularza");

            lastPerson = GetLatestFormSaved(new string[] { Paths.XML_FOLDER_PATH, Paths.JSON_FOLDER_PATH});

            if (lastPerson != null)
            {
                Console.WriteLine("Imię: " + lastPerson.Name);
                Console.WriteLine("Nazwisko: " + lastPerson.Surname);
                Console.WriteLine("Wiek: " + lastPerson.Age);
                Console.WriteLine("Data urodzenia: " + lastPerson.DateOfBirth.ToString("dd.MM.yyyy"));
                Console.WriteLine("Płeć: " + lastPerson.Sex.ToString());
                Console.WriteLine("Żonaty/Zamężna: " + (lastPerson.IsMarried == false ? "Nie" : "Tak"));
            }
            else
                Console.WriteLine("Brak ostatnio dodanego formularza");
        }

        private Person ReadXmlFile()
        {
            var DLLXml = Assembly.LoadFile(Paths.XML_DLL_PATH);
            Person lastPerson = null;

            if (DLLXml != null)
            {
                Type libraryType = DLLXml.GetExportedTypes().FirstOrDefault(x => x.Name == "Xml");

                if (libraryType != null)
                {
                    var libType = Activator.CreateInstance(libraryType);
                    var method = libraryType.GetMethod("Deserialize");

                    if (method != null)
                    {
                        lastPerson = (Person)method.Invoke(libType, null);
                    }
                }
            }

            return lastPerson;
        }

        private Person ReadJsonFile()
        {
            var DLLJson = Assembly.LoadFile(Paths.JSON_DLL_PATH);
            Person lastPerson = null;

            if (DLLJson != null)
            {
                Type libraryType = DLLJson.GetExportedTypes().FirstOrDefault(x => x.Name == "Json");

                if (libraryType != null)
                {
                    var libType = Activator.CreateInstance(libraryType);
                    var method = libraryType.GetMethod("Deserialize");

                    if (method != null)
                    {
                        lastPerson = (Person) method.Invoke(libType, null);
                    }
                }
            }

            return lastPerson;
        }

        private void SaveXmlFile(Person person)
        {
            var DLL = Assembly.LoadFile(Paths.XML_DLL_PATH);

            if (DLL != null)
            {
                Type libraryType = DLL.GetExportedTypes().FirstOrDefault(x => x.Name == "Xml");

                if (libraryType != null)
                {
                    var libType = Activator.CreateInstance(libraryType);
                    var method = libraryType.GetMethod("Serialize");

                    if (method != null)
                    {
                        method.Invoke(libType, new object[] { person });
                    }
                }
            }
        }

        private void SaveJsonFile(Person person)
        {
            var DLL = Assembly.LoadFile(Paths.JSON_DLL_PATH);

            if (DLL != null)
            {
                Type libraryType = DLL.GetExportedTypes().FirstOrDefault(x => x.Name == "Json");

                if (libraryType != null)
                {
                    var libType = Activator.CreateInstance(libraryType);
                    var method = libraryType.GetMethod("Serialize");

                    if (method != null)
                    {
                        method.Invoke(libType, new object[] { person });
                    }
                }
            }
        }
    }
}
