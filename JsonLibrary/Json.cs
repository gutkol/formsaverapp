﻿using FormApp.Classes.Model;
using FormApp.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonLibrary
{
    public class Json : ISerializePlugin
    {
        public string Name => "JsonLibrary";

        public string Description => "Plugin ten służy do zapisu i odczytu pliku w formacie json.";

        public Person Deserialize()
        {
            Person person = new Person();
            string filePath = Path.Combine(Paths.JSON_FOLDER_PATH);
            string[] files = null;

            try
            {
                files = Directory.GetFiles(filePath);
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory(Paths.JSON_FOLDER_PATH);
            }

            if (files != null)
            {
                string lastFile = files[files.Length - 1].Split('\\').LastOrDefault();

                JsonSerializer deserializer = new JsonSerializer();

                using (StreamReader sr = new StreamReader(Paths.JSON_FOLDER_PATH + lastFile))
                {
                    using (JsonReader reader = new JsonTextReader(sr))
                    {
                        person = (Person)deserializer.Deserialize(reader, typeof(Person));
                    }
                }
            }

            return person;
        }

        public void Serialize(Person person)
        {
            JsonSerializer serializer = new JsonSerializer();

            using (StreamWriter sw = new StreamWriter(Paths.JSON_FOLDER_PATH + person.ID + ".txt"))
            {
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, person);
                }
            }
        }
    }
}
